<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<br>
<h2>New Centre Added on GST VSK</h2>
<hr><br>
Name: <?= $data['name'] ?> <br><br>
Phone Number: <?= $data['contact_number'] ?> <br><br>
Address:<br><?= $data['address_line1'] ?> <br><br>
City:<br><?= $data['city'] ?> <br><br>
State:<br><?= $data['state'] ?> <br><br>
Pincode:<br><?= $data['pincode'] ?> <br><br>
------------------
<br>
Please do not reply to this mail id.
