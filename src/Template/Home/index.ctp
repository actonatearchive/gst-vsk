<?= $this->Flash->render() ?>

<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<!-- <script src="https://code.highcharts.com/mapdata/countries/in/in-all.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.6/proj4.js"></script>
<?= $this->Html->script('map/all-geo.js') ?>
<div class="container counter_row">
	<div class="row dashboard_row">
		<div class="col-sm-12 col-lg-4 col-md-12">
			<div class="row">
				<div class="col col-6 col-lg-4 col-md-6 col-sm-6 counter">
						<div class="d-flex justify-content-center align-items-center flex-column counter_item">
							<!-- <img src="img/city.svg" alt=""> -->
                            <?= $this->Html->image(
                              'city.svg'
                            ) ?>

							<h1 class="count"><?= $center_count ?></h1>
							<h6>Centers</h6>
						</div>
					</div>
					<!-- <div class="col col-6 col-lg-6 col-md-6 col-sm-6 counter">
						<div class="d-flex justify-content-center align-items-center flex-column counter_item">
						    <?= $this->Html->image(
                              'team.svg'
                            ) ?>

							<h1 class="count"><?= $center_count * 5 ?></h1>
							<h6>Work Force</h6>
						</div>
					</div> -->
					<div class="col col-6 col-lg-4 col-md-6 col-sm-6 counter">
						<div class="d-flex justify-content-center align-items-center flex-column counter_item">
							<!-- <img src="img/event.svg" alt=""> -->
                            <?= $this->Html->image(
                              'location.svg'
                            ) ?>

							<h1 class="count"><?= $state_count ?></h1>
							<h6>States</h6>
						</div>
					</div>
					<div class="col col-6 col-lg-4 col-md-6 col-sm-6 counter">
						<div class="d-flex justify-content-center align-items-center flex-column counter_item">
							<!-- <img src="img/location.svg" alt=""> -->
                            <?= $this->Html->image(
                              'location.svg'
                            ) ?>

							<h1 class="count"><?= $city_count?></h1>
							<h6>Cities</h6>
						</div>
					</div>
				</div>
		</div>
		<div class="col-sm-12 col-lg-8 col-md-12">
			<div id="map"></div>
		</div>
	</div>
</div>
		<div id="marker-tooltip"></div>

		<!-- <div class="container counter_row">
			<div class="container">
				<div class="row">

				</div>
			</div>
		</div> -->

		<!-- <div class="container info_block">
			<div class="container">
				 <h1>GST Suvidha Kendra</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div> -->



<!-- Scripts started -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
        $('.count').counterUp({
            delay: 10,
            time: 1000
        });
    });
</script>
<script type="text/javascript">
(function () {
var data = [
	{
		name: 'Gujarat',
		count: 50
	}
];

	var city_data = JSON.parse('<?= $city_json?>');
	var state_data = JSON.parse('<?= $state_json?>');

	// state_data.push({
	// 	name: 'Punjab',
	// 	count: 10000
	// })
console.log("State:",state_data);
// Create the chart
Highcharts.mapChart('map', {
    chart: {
        map: 'countries/in/in-all',
        backgroundColor: 'transparent'
    },

    title: {
        text: ''
    },
    legend: {
    	enabled: false
    },
    subtitle: {
        text: ''
    },
    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom',
            align: 'right'
        }
    },
    credits: {
    	enabled: false
    },
    colorAxis: {
        min: 0
    },

    series: [{
        data: state_data,
        nullColor: '#d7ccff',
        color: '#b39ff9',
        joinBy: ['name', 'name'],
        name: 'States',
        states: {
            hover: {
                color: '#eae5fc'
            }
        },
        dataLabels: {
            enabled: false,
            format: '{point.name}'
        },
        tooltip: {
        	headerFormat: '',
    	    pointFormat: '<b>{point.name}</b>: {point.count}'
	    },
    },
    {
    	type: 'mappoint',
    	name: "Cities",
    	data: city_data,
    	color: '#593eb9',
    	dataLabels: {
    		enabled: true,
    		format: '{point.name}'
    	},
	    tooltip: {
        	headerFormat: '',
    	    pointFormat: '<b>{point.name}</b>: {point.count}'
	    },

    }]
});

})();
</script>
<!-- <script type="text/javascript">
	(function () {

	    var pos = new google.maps.LatLng(22.19443,71.3642343);
		var marker = []; //For Multiple Markers
		var infowindow = []; //For Multiple Info Windows.
		var user_data = '<?= $json_data ?>';

		var user_details = JSON.parse(user_data);
	    var map = new google.maps.Map(document.getElementById('map'), {
	        center: pos,
	        zoom: 8,
            scrollwheel: false,
	        mapTypeId: google.maps.MapTypeId.ROAD,
	        styles: [
					  {
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#f5f5f5"
					      }
					    ]
					  },
					  {
					    "elementType": "labels.icon",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#616161"
					      }
					    ]
					  },
					  {
					    "elementType": "labels.text.stroke",
					    "stylers": [
					      {
					        "color": "#f5f5f5"
					      }
					    ]
					  },
					  {
					    "featureType": "administrative.land_parcel",
					    "elementType": "labels",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "administrative.land_parcel",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#bdbdbd"
					      }
					    ]
					  },
					  {
					    "featureType": "poi",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#eeeeee"
					      }
					    ]
					  },
					  {
					    "featureType": "poi",
					    "elementType": "labels.text",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "poi",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#757575"
					      }
					    ]
					  },
					  {
					    "featureType": "poi.business",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "poi.park",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#e5e5e5"
					      }
					    ]
					  },
					  {
					    "featureType": "poi.park",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#9e9e9e"
					      }
					    ]
					  },
					  {
					    "featureType": "road",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "road",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#ffffff"
					      }
					    ]
					  },
					  {
					    "featureType": "road",
					    "elementType": "labels.icon",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "road.arterial",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#757575"
					      }
					    ]
					  },
					  {
					    "featureType": "road.highway",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#dadada"
					      }
					    ]
					  },
					  {
					    "featureType": "road.highway",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#616161"
					      }
					    ]
					  },
					  {
					    "featureType": "road.local",
					    "elementType": "labels",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "road.local",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#9e9e9e"
					      }
					    ]
					  },
					  {
					    "featureType": "transit",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "transit.line",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#e5e5e5"
					      }
					    ]
					  },
					  {
					    "featureType": "transit.station",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#eeeeee"
					      }
					    ]
					  },
					  {
					    "featureType": "water",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#c9c9c9"
					      }
					    ]
					  },
					  {
					    "featureType": "water",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#9e9e9e"
					      }
					    ]
					  }
					]
	    });

		var icon = {
		    url: baseUrl + "img/map-mark.svg", // url
		    scaledSize: new google.maps.Size(34, 34), // scaled size
		    origin: new google.maps.Point(0,0), // origin
		    anchor: new google.maps.Point(0, 0) // anchor
		};

		var j = 0;
		user_details.forEach(function(row) {

			var _pos = {lat: parseFloat(row.lat), lng: parseFloat(row.lng)};
			setTimeout(function() {
				addMarker(j,_pos,map,row); //Sending Index, Position, Map ID, and Data
                j ++;
			}, 100 * j);

		});


		function addMarker(i,latlng,map,_data) {
	        marker[i] = new google.maps.Marker({
	               position: latlng,
	               map: map,
	               title: _data.name,
	               icon: icon,
				   animation: google.maps.Animation.DROP
	           });
	           console.log("Data: ",_data);
	       //AddInfo to particular marker
	        addInfoWindow(i,map,marker[i],latlng,_data);
	   }


	   function addInfoWindow(_i,map,marker,_pos,_data) {

		//   var contentString = "<strong>"+ _data.name+"</strong><br>";
		  var contentString = _data.address_line1 + ","+ _data.address_line2 + ", " + _data.city + ", " + _data.state +" "+ _data.pincode +".<br>";
		//   contentString += "Mob : "+_data.contact_number;

		  	infowindow[_i] = new google.maps.InfoWindow({
			  content: contentString,
			  maxWidth: 200
		  	});

			marker.addListener('click', function() {
				   // infowindow[_i].open(map, marker);
				   if(!marker.open){
					   infowindow[_i].open(map,marker);
					   marker.open = true;
				   }
				   else{
					   infowindow[_i].close();
					   marker.open = false;
				   }
				   google.maps.event.addListener(map, 'click', function() {
					   infowindow[_i].close();
					   marker.open = false;
				   });
			});
   }

        // var reliance_pos = new google.maps.LatLng(22.293651,73.1841814);
	    // var marker = new google.maps.Marker({
	    //     position: reliance_pos,
	    //     map: map,
	    //     icon: icon
	    // });
	    // marker.tooltipContent = '';
	    // var infoWindow = new google.maps.InfoWindow({
	    //     content: '<strong>Reliance Mega Mall</strong><br> Old Padra Rd, Dev Deep Nagar, Muj Mahuda, Vadodara, Gujarat 390020.<br>Mob : 02652972916',
	    //     maxWidth: 200
	    // });
	    // google.maps.event.addListener(marker, 'click', function () {
	    //     infoWindow.open(map, marker);
	    // });
	    // google.maps.event.addListener(marker, 'mouseout', function () {
	    //     $('#marker-tooltip').hide();
	    // });

		console.log("UserData:",user_data);
	})();

	function fromLatLngToPoint(latLng, map) {
	    var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
	    var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
	    var scale = Math.pow(2, map.getZoom());
	    var worldPoint = map.getProjection().fromLatLngToPoint(latLng);
	    return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
	}


    function addMarker(map, pos, icon)
    {
        var marker = new google.maps.Marker({
	        position: pos,
	        map: map,
	        icon: icon
	    });

        return marker;
    }
</script> -->
