<?= $this->Flash->render() ?>
<div class="map_area small">
	<div id="map" style="height: 250px !important;"></div>
	<div id="marker-tooltip"></div>
</div>

<?= $this->Form->create(null, ['method'=>'post','url' => ['controller'=>'home','action' => 'add_center']]) ?>
<div class="container-fluid info_block">
	<div class="container">
		<div class="row no-gutters">
			<div class="col">
				<div class="form-group">
				    <label for="username">Name</label>
				    <input type="text" name="name" class="form-control" id="username" placeholder="Type your name here" required>
			  	</div>
				<div class="form-group">
				    <label for="contact_num">Contact</label>
				    <input type="text" name="contact_number" class="form-control" id="contact_num" placeholder="Contact number" required>
			  	</div>
				<div class="form-group">
				    <label for="address_line1">Address Line 1</label>
				    <textarea class="form-control" name="address_line1" id="address_line1" rows="6" placeholder="Type your address here" required></textarea>
			  	</div>

				<div class="form-group">
					<label for="address_line2">Address Line 2</label>
					<textarea class="form-control" name="address_line2" id="address_line2" rows="6" placeholder="Type your address here"></textarea>
				</div>

				<div class="form-group">
					<label for="city">City</label>
					<input type="text" name="city" class="form-control" id="city" placeholder="Type your city here" required>
				</div>

				<div class="form-group">
					<label for="state">State</label>
					<input type="text" name="state" class="form-control" id="state" placeholder="Type your state here" required>
				</div>

				<div class="form-group">
					<label for="pincode">Pincode</label>
					<input type="text" name="pincode" class="form-control" id="pincode" placeholder="Type your pincode here" required>
				</div>

                <input id="lat" type="hidden" name="lat" class="form-control" />
                <input id="lng" type="hidden" name="lng" class="form-control" />

			</div>
		</div>
	</div>
</div>
<div class="row no-gutters">
	<div class="col">
		<button id="submit-btn" type="submit" class="btn btn-primary btn-lg btn-block">
			submit
		</button>
	</div>
</div>
</form>

<script type="text/javascript">
    var user_pos = {
        lat: false,
        lng: false
    };
	(function () {

	    var default_pos = new google.maps.LatLng(22.19443,71.3642343);

	    var map = new google.maps.Map(document.getElementById('map'), {
	        center: default_pos,
	        zoom: 12,
	        mapTypeId: google.maps.MapTypeId.ROAD,
	        styles: [
					  {
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#f5f5f5"
					      }
					    ]
					  },
					  {
					    "elementType": "labels.icon",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#616161"
					      }
					    ]
					  },
					  {
					    "elementType": "labels.text.stroke",
					    "stylers": [
					      {
					        "color": "#f5f5f5"
					      }
					    ]
					  },
					  {
					    "featureType": "administrative.land_parcel",
					    "elementType": "labels",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "administrative.land_parcel",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#bdbdbd"
					      }
					    ]
					  },
					  {
					    "featureType": "poi",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#eeeeee"
					      }
					    ]
					  },
					  {
					    "featureType": "poi",
					    "elementType": "labels.text",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "poi",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#757575"
					      }
					    ]
					  },
					  {
					    "featureType": "poi.business",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "poi.park",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#e5e5e5"
					      }
					    ]
					  },
					  {
					    "featureType": "poi.park",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#9e9e9e"
					      }
					    ]
					  },
					  {
					    "featureType": "road",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "road",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#ffffff"
					      }
					    ]
					  },
					  {
					    "featureType": "road",
					    "elementType": "labels.icon",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "road.arterial",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#757575"
					      }
					    ]
					  },
					  {
					    "featureType": "road.highway",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#dadada"
					      }
					    ]
					  },
					  {
					    "featureType": "road.highway",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#616161"
					      }
					    ]
					  },
					  {
					    "featureType": "road.local",
					    "elementType": "labels",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "road.local",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#9e9e9e"
					      }
					    ]
					  },
					  {
					    "featureType": "transit",
					    "stylers": [
					      {
					        "visibility": "off"
					      }
					    ]
					  },
					  {
					    "featureType": "transit.line",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#e5e5e5"
					      }
					    ]
					  },
					  {
					    "featureType": "transit.station",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#eeeeee"
					      }
					    ]
					  },
					  {
					    "featureType": "water",
					    "elementType": "geometry",
					    "stylers": [
					      {
					        "color": "#c9c9c9"
					      }
					    ]
					  },
					  {
					    "featureType": "water",
					    "elementType": "labels.text.fill",
					    "stylers": [
					      {
					        "color": "#9e9e9e"
					      }
					    ]
					  }
					]
	    });
		var icon = {
		    url: baseUrl + "img/map-mark.svg", // url
		    scaledSize: new google.maps.Size(34, 34), // scaled size
		    origin: new google.maps.Point(0,0), // origin
		    anchor: new google.maps.Point(0, 0) // anchor
		};


        var infoWindow = new google.maps.InfoWindow;

        // GeoLocation Code -----------------------------------------------------------
            if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };
                console.log("Location Found: ");
                console.log("Lat: ",position.coords.latitude," Long: ", position.coords.longitude);
                user_pos.lat = position.coords.latitude;
                user_pos.lng = position.coords.longitude;

                var marker = addMarker(map, pos, icon);
                getCoordinateInfo(user_pos.lat, user_pos.lng);
              }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
              });
            } else {
              // Browser doesn't support Geolocation
            //   alert("Oops! Unable to detect your location.");

              handleLocationError(false, infoWindow, map.getCenter());
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                                     'Error: The Geolocation service failed.' :
                                     'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
                alert("Oops! Unable to detect your location.");
            }

        // ---- GeoLocation Code Over here --------------------------------------------



	})();

	function fromLatLngToPoint(latLng, map) {
	    var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
	    var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
	    var scale = Math.pow(2, map.getZoom());
	    var worldPoint = map.getProjection().fromLatLngToPoint(latLng);
	    return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
	}

    function addMarker(map, pos, icon)
    {

        var marker = new google.maps.Marker({
	        position: pos,
	        map: map,
	        icon: icon
	    });
		map.setZoom(15);
		map.panTo(marker.getPosition());
        return marker;
    }


    function getCoordinateInfo(lat,long)
    {
    	console.log("lat:",lat);
    	console.log("long:",long);

    	var url = "https://maps.googleapis.com/maps/api/geocode/json?&latlng=" + lat +","+long;
    	$.get( url, function( data ) {
		  // $( ".result" ).html( data );
		  console.log("Response: ",data);

		    var mArr = data.results[0].address_components;
            if(mArr && mArr[mArr.length - 2].long_name !== undefined) {
                var city = mArr[mArr.length - 4].long_name;
                var state = mArr[mArr.length - 3].long_name;
                var pincode = mArr[mArr.length - 1].long_name;

                console.log(state);
                console.log(city);
                console.log(pincode);

                if(state !== "India") {
                    $('#state').val(state);
                    $('#city').val(city);
                    $('#pincode').val(pincode);
                }
            }

		});
    }
</script>

<script>
    function checkAllTheFields()
    {

        var name = document.getElementById('username').value;
        var contact = document.getElementById('contact_num').value;
        var address_line1 = document.getElementById('address_line1').value;
        // console.log("Name: ", name);
        if (name && name === "" || (name.trim()).length < 3) {
            //False Check
            // console.log("Check 1 failed.");
            return false;
        }

        if (contact && contact === "" || (contact.trim()).length < 3) {
            // console.log("Check 2 failed.");
            return false;
        }

        if (address_line1 && address_line1 === "" || (address_line1.trim()).length < 3) {
            // console.log("Check 3 failed.");
            return false;
        }

        if (user_pos && user_pos.lat === false && user_pos.lng === false) {
            // console.log("Check 4 failed.");
            return false;
        }
        return true;
    }


    setInterval(function() {
        var flg = checkAllTheFields();

        if (flg === true) {
            document.getElementById("submit-btn").disabled = false;

            document.getElementById('lat').value = user_pos.lat;
            document.getElementById('lng').value = user_pos.lng;
            // console.log("Enabled.");
        } else {
            document.getElementById("submit-btn").disabled = true;
            // console.log("Disabled.");
        }
    },500);
</script>
