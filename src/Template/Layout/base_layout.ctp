<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Gst-Vsk</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Bootstrap CSS -->
	<!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
	<!-- Base CSS -->
	<!-- <link href="css/style.css" rel="stylesheet"> -->
    <?= $this->Html->css(['bootstrap.min','style']) ?>

	<!-- Google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500" rel="stylesheet">

    <?= $this->Html->script(
        [
            'jquery-3.1.1.slim.min',
            'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js',
            'tether.min',
            'bootstrap.min',
            'https://maps.googleapis.com/maps/api/js?key=AIzaSyDpUbAd2JvguzuzhNxeRlNvc2mEaeklTQg&extension=.js',
            'waypoints.min',
            'jquery.counterup.min'
        ]
    )?>
<script>
    var baseUrl = "<?= $this->request->webroot; ?>";
</script>

</head>
<body>
	<div class="modal fade" id="contact_form" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">

      <?= $this->Form->create(null, ['url' => ['controller'=>'home','action' => 'send_mail']]) ?>
        <div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Contacts us</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
				    <label for="exampleTextarea">Name</label>
				    <input type="text" name="name" class="form-control" id="name" placeholder="Type your name here" required>
			  	</div>
				<div class="form-group">
				    <label for="exampleTextarea">Email</label>
				    <input type="email" name="email" class="form-control" id="contact" placeholder="Type your email number" required>
			  	</div>
				<div class="form-group">
				    <label for="exampleTextarea">Message</label>
				    <textarea class="form-control" name="message" id="message" rows="6" placeholder="Type your message here" required></textarea>
			  	</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-lg btn-block">Save</button>
			</div>
	    </div>
        </form>
	  </div>
	</div>
	<div class="container">
		<nav class="navbar navbar-light navbar-toggleable-md">
			<div class="d-flex justify-content-between">
			  	<a class="navbar-brand" href="<?= $this->Url->build(['controller'=>'home','action'=>'index']) ?>">Gst-Vsk</a>
			  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="true" aria-label="Toggle navigation">
		    		<span class="navbar-toggler-icon"></span>
		  		</button>
	  		</div>
			<div class="collapse navbar-collapse" id="main-nav">
				<ul class="nav col justify-content-lg-end justify-content-sm-end justify-content-md-end">
					<li class="nav-item active">
						<a class="nav-link" href="<?= $this->Url->build(['controller'=>'home','action'=>'index']) ?>">Dashboard</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#" data-toggle="modal" data-target="#contact_form">Contact</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
<?= $this->fetch('content') ?>

    <div class="container">
		<div class="container footer">
			<div class="row">
				<div class="col col-12 col-lg-6 col-md-6 col-sm-6 align-content-center flex-wrap">
					<div class="d-flex justify-content-center justify-content-lg-start justify-content-sm-start justify-content-md-start">
                        <?= $this->Html->image(
                          'venvik-logo.png',
                            [
                              "alt" => "Venvik Tech Solutions",
                              "title" => "Venvik Tech Solutions"
                            ]
                        ) ?>
						<div class="copyright d-flex">&copy; <?= date('Y')?> Venvik Tech Solutions</div>
					</div>

				</div>
				<div class="col col-12 col-lg-6 col-md-6 col-sm-6 align-content-center flex-wrap">
					<!-- <div class="d-flex justify-content-center justify-content-lg-end justify-content-sm-end justify-content-md-end">
                        <?= $this->Html->image(
                          'gstn-logo.png',
                            [
                              "alt" => "Goods and Services Tax Network",
                              "title" => "Goods and Services Tax Network"
                            ]
                        ) ?>

                        <?= $this->Html->image(
                          'cdsl-logo.png',
                            [
                              "alt" => "CDSL - Your Depository",
                              "title" => "CDSL - Your Depository"
                            ]
                        ) ?>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<script>
setTimeout($('.message').fadeOut(7000), 0);
</script>
