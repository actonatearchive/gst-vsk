<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Home
 * @package   Home
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2014-2016 Copyright (c) LetsShave Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller;


use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Home Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @category Home
 * @package  Home
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */
class HomeController extends AppController
{
    /**
     *   Initialization
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }

   /**
   * Home Index
   *
   * @return void
   */
   public function index()
   {
       $this->viewBuilder()->layout("base_layout");
       $this->loadComponent("User");

       // $details = $this->User->getUserDetails();

       $details = $this->User->getStateWiseData();
       $city = $this->User->getCityWiseData();
       $center_count = $this->User->getCenterCount();
       // pr($city);
       // pr($details); die();

       $city_count = sizeof($city);
       $state_count = sizeof($details);

       $city_json = json_encode($city);
       $state_json = json_encode($details);

       // $details = json_encode($details);
       // $details = str_replace("'","\'",$details);
       // $details = str_replace('"','\"',$details);

       $this->set('json_data', $details);
       $this->set('city_json', $city_json);
       $this->set('state_json', $state_json);
       $this->set('city_count', $city_count);
       $this->set('state_count', $state_count);

       $this->set('center_count', $center_count);


   }


   /**
   * Form Submission
   *
   * @return void
   */
   public function addCenter()
   {
       $this->viewBuilder()->layout("base_layout");

       if ($this->request->is('post')) {
           $data = $this->request->data;

           $this->loadComponent('User');
           $this->loadComponent('Email');

           $chk = $this->User->add($data);

           if ($chk != false) {
               $this->Flash->success(
                    __(
                        "Your details are submitted."
                    )
                );

                $this->Email->send(
                    'add_centre',
                    'vsk@venvik.com',
                    'Centre Added on GST VSK',
                    [
                        'data' => $data
                    ],
                    'mohammed.sufyan@actonate.com'
                );

                return $this->redirect(
                    ['controller'=>'home','action'=>'add_center']
                );
           } else {
               $this->Flash->error(
                    __(
                        "Oops!! Please try again later."
                    )
                );
           }
       }
   }


   /**
   *    Send Mail
   *
   * @return void
   */
   public function sendMail()
   {
       if ($this->request->is('post')) {
           $data = $this->request->data;

           $this->loadComponent('Email');

           $this->Email->send(
               'contact_us',
               'vsk@venvik.com',
               'Contact Us request on GST VSK',
               [
                   'data' => $data
               ],
               'mohammed.sufyan@actonate.com'
           );
           $this->Flash->success(
                __(
                    "Mail has been sent."
                )
            );
           return $this->redirect(['controller'=>'home', 'action'=>'index']);
       }
   }
}

?>
