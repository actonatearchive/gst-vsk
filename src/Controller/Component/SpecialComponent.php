<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   Special
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2014-2016 Copyright (c) LetsShave Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;

/**
 * Special Component
 *
 * @category Component
 * @package  Special
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.letsshave.com/
 */

class SpecialComponent extends Component
{
    /**
    *  Universal Unique Indentifier v4
    *    DATE: 30th January 2017
    *
    * @return uuid
    * @author Mohammed Sufyan <mohammed.sufyan@actonate.com>
    */
   public static function UUIDv4()
   {
       return sprintf(
           '%s-%04x-%04x-%04x-%04x%04x%04x',
           substr(uniqid('', true), 0, 8), //UUIDv1 Based on time
           mt_rand(0, 0xffff),
           mt_rand(0, 0x0fff) | 0x4000,
           mt_rand(0, 0x3fff) | 0x8000,
           mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
       );
   }
}
