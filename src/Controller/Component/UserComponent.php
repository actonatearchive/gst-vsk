<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 5
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   User
 * @author    Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @copyright 2014-2016 Copyright (c) LetsShave Pvt. Ltd.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

/**
 * User Component
 *
 * @category Component
 * @package  User
 * @author   Mohammed Sufyan Shaikh <mohammed.sufyan@actonate.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.actonate.com/
 */

class UserComponent extends Component
{
    public $components = ['Special'];

    /**
    *  Add User Details
    *
    *
    * @return void
    */
    public function add($data = [])
    {
        $user_details = TableRegistry::get('UserDetails');

        $user = $user_details->newEntity($data);

        if ($out = $user_details->save($user)) {
            return $out;
        }
        return false;
    }


    /**
    * Get User Details
    *
    * @return array
    */
    public function getUserDetails()
    {
        $user_details = TableRegistry::get("UserDetails");

        $tmp = $user_details->find('all')
            ->toArray();
        return $tmp;
    }

    /**
    *   Get Center count
    *
    * @return count
    */
    public function getCenterCount()
    {
        $user_details = TableRegistry::get("UserDetails");

        $tmp = $user_details->find('all');

        $count = $tmp->count();
        return $count;
    }


    /**
    *  Get State Wise Data
    *
    * @return array
    */
    public function getStateWiseData()
    {
        $user_details = TableRegistry::get("UserDetails");

        $tmp = $user_details->find('all')
        ->select([
            'state',
            'count' => $user_details->find()->func()->count('*')
        ])
        ->group('state')
        ->limit(100)
        ->toArray();

        $data = [];
        foreach ($tmp as $key => $value) {
            $tmp2 = [];

            $tmp2['count'] = $value['count'];
            $tmp2['name'] = $value['state'];

            array_push($data,$tmp2);

        }


        return $data;
    }


    /**
    *  Get City Wise Data
    *
    * @return array
    */
    public function getCityWiseData()
    {
        $user_details = TableRegistry::get("UserDetails");
        $cities = TableRegistry::get("Cities");

        $tmp = $user_details->find('all')
        ->select([
            'UserDetails.city',
            'count' => $user_details->find()->func()->count('*'),
        ])
        ->group('UserDetails.city')
        ->limit(100)
        ->toArray();

        $data = [];
        foreach ($tmp as $key => $value) {
            $tmp2 = [];
            $tmp2['name'] = $value['city'];
            $tmp2['count'] = $value['count'];
            $cty = $cities->findByCityName($value['city'])->first();

            $tmp2['lat'] = $cty['latitude'];
            $tmp2['lon'] = $cty['longitude'];

            array_push($data,$tmp2);
        }

        return $data;
    }
}
